// ==UserScript==
// @name        GimmeLink - wbijam.pl
// @namespace   Return to Luna Scripts
// @match       https://*.wbijam.pl/*
// @grant       none
// @version     1.1.1
// @author      mlunax
// @description 5/5/2021, 6:58:17 PM
// @include https://*.wbijam.pl/*
// @run-at document-body
// ==/UserScript==

const style = document.createElement("style");
style.innerText = `
.odtwarzacz_link + button {
    display:none;
}

.lista_hover:hover button {
    display:block;
}
`;
document.head.appendChild(style);
for (const el of document.querySelectorAll("span.odtwarzacz_link")) {
    const but = document.createElement("button");
    but.innerText = "Gimme video link";
    but.onclick = () => {
        const url = el.getAttribute("rel");
        fetch(`https://${document.location.host}/odtwarzacz-${url}.html`).then(e => {
            e.text().then(t => {
            const dom = new DOMParser().parseFromString(t, "text/html");
                const h = dom.querySelector('iframe[src*="cda.pl"]');
                const vkh = dom.querySelector('span[class="odtwarzaj_vk"]');
                if (h){
                    alert(h.src);
                } else if (vkh) {
                  const vkUrl = "https://vk.com/video" + vkh.getAttribute("rel") + "_" + vkh.id;
                  alert(vkUrl);
                }
            })
        })
    };
    el.parentElement.appendChild(but);
}
