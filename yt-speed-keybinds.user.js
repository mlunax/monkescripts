// ==UserScript==
// @name        html5 video speed controller (vlc like)
// @namespace   gitlab.com/mlunax
// @version     1.1.4
// @description Simple html5 video speed control with '[', ']', and ''' (vlc like). '[': decrease by 0.1, '+]: increase by 0.1, ''': back to 1.0x
// @author      https://gitlab.com/mlunax
// @match       https://www.youtube.com/*
// @match       https://www.bilibili.com/*
// @match       *://*.zhihuishu.com/*
// @grant       none
// ==/UserScript==
// fork of https://greasyfork.org/en/scripts/421464-html5-video-speed-controller-vlc-like

(function() {
    'use strict';
    let firstRun = true;
    function setPlaybackRate(player, rate) {
        if (rate < 0.1) rate = 0.1;
        else if (rate > 10) rate = 10;
        player.playbackRate = rate;
        console.log("playing in %sx", (rate).toFixed(1));
    }

    window.addEventListener('keypress', function(event) {
        var player = document.querySelector("video");
        var curRate = Number(player.playbackRate);
        var rate = player.playbackRate;
        // vlc actually uses '[' and ']', but they are used by vimium
        if (event.key == "[") {
            console.log("[ pressed");
            setPlaybackRate(player, curRate - 0.1);

        } else if(event.key == "]") {
            console.log("] pressed");
            setPlaybackRate(player, curRate + 0.1);
        } else if(event.key == "\'") {
            console.log("' pressed");
            setPlaybackRate(player, 1);
        }
        rate = player.playbackRate;
        if (firstRun){
          let infoText =  document.createElement("h1");
          infoText.id = "infoText_speedcontroler";
          infoText.innerText = (rate).toFixed(1) + "x playback rate";
          document.querySelector("ytd-watch-metadata").querySelector("#actions").appendChild(infoText);
        } else {
          document.querySelector("#infoText_speedcontroler").innerText = (rate).toFixed(1) + "x playback rate";
        }
        firstRun = false;

    });

})();
