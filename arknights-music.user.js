// ==UserScript==
// @name        ArknightsMusicJson
// @namespace   Return to Luna Scripts
// @match       https://monster-siren.hypergryph.com/*
// @grant       none
// @version     1.1.2
// @grant       GM.notification
// @author      mlunax
// @description 4/29/2021, 11:07:11 PM
// @run-at document-end
// @license MIT
// ==/UserScript==
const triggerFunction = (async () => {
let cids = [];
if (GM && GM.notification){
	GM.notification({text:"Script is running"});
} else {
	alert("After closing this popout, script will be in the running")
}
for (const album of await fetch("/music").then(e => e.text().then(v => JSON.parse(v.match(/window\.g_initialProps = (\{.+\})/)[1].replace(/undefined/g,'""'))['music']['albumList']))){
     const req = await fetch(`/api/album/${album.cid}/detail`);
     const json = await req.json();
     for (const s of json.data.songs){
        const songCid = s.cid;
        const songReq = await fetch('/api/song/' + songCid);
        const songJson = await songReq.json()
        const songUrl = songJson.data.sourceUrl;
        const songName = songJson.data.name;
        cids.push({songCid: songCid, name: songName, url: songUrl});
     }
}
const ahref = document.createElement("a")
ahref.href = "data:application/json," + encodeURIComponent(JSON.stringify(cids))
ahref.setAttribute("download", "arknights" + ".json")
ahref.click()
});
const but = document.createElement("button");
but.innerText = "Run Luna Script";
but.onclick = triggerFunction;
document.querySelector("header").appendChild(but);
