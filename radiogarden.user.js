// ==UserScript==
// @name        LunarScrapping- radio.garden
// @namespace   Return to Luna Scripts
// @match       http://radio.garden
// @grant       none
// @version     1.0.0
// @author      mlunax
// @description 4/29/2021, 11:07:11 PM
// @include http://radio.garden/*
// @license MIT
// ==/UserScript==


const originalFetch = fetch;
fetch = async function(data, options){
    let ret;
    if (typeof data == "string" && data.startsWith("/api/ara/content/channel/")){
        let a;
        ret = await originalFetch(data,options).then(async v => {
          a = await v.clone().json(); 
          return v;
        });
        const url = "http://radio.garden/api/ara/content/listen/" + a.data.id + "/channel.mp3";
        console.log(`%c${url}`, "color:red");
        return ret;
    }
    return originalFetch(data,options);
}
